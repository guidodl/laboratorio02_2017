package it.unimi.di.sweng.lab02;

public interface Bowling {
	public void roll(int pins);				//lancio birilli
	public int score();
}
